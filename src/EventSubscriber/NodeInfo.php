<?php

namespace Drupal\nodeinfo\EventSubscriber;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class NodeInfo.
 */
class NodeInfo implements EventSubscriberInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The route admin context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * The route admin context.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The string translation.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * NodeInfo constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The routematch.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The route admin context service.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The dateformatter service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation manager.
   */
  public function __construct(RouteMatchInterface $route_match, AccountProxy $current_user, AdminContext $admin_context, DateFormatter $dateFormatter, TranslationInterface $string_translation) {
    $this->routeMatch = $route_match;
    $this->currentUser = $current_user;
    $this->adminContext = $admin_context;
    $this->dateFormatter = $dateFormatter;
    $this->stringTranslation = $string_translation;
  }

  /**
   * Display nodeinfo message for currently viewed node.
   */
  public function nodeinfo() {

    $node = $this->routeMatch->getParameter('node');

    if (is_object($node) && !$this->adminContext->isAdminRoute() && $this->currentUser->hasPermission('view node info')) {
      $type = 'status nodeinfo';
      $pub_status = 'published';
      if (!$node->isPublished()) {
        $pub_status = 'unpublished';
        $type = 'warning';
      }

      $nodeinfo = $this->stringTranslation->translate('Displaying @status <strong @attrs>node/@nid</strong> of type <strong>@ct</strong> last modified <strong>@modified</strong>',
        [
          '@status' => $pub_status,
          '@attrs' => 'class="selectme" contenteditable="true" onclick="document.execCommand(\'selectAll\',false,null)"',
          '@nid' => $node->id(),
          '@ct' => $node->getType(),
          '@modified' => $this->dateFormatter->format($node->getChangedTime()),
        ]
      );

      drupal_set_message($nodeinfo, $type, FALSE);

      if ($node->isPromoted()) {
        drupal_set_message($this->stringTranslation->translate('This node is <em>promoted to frontpage</em>'), $type, FALSE);
      }

      if ($node->isSticky()) {
        drupal_set_message($this->stringTranslation->translate('This node is marked <em>Sticky at top of lists</em>'), $type, FALSE);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Dynamic cache subscribes an event with priority 27, trigger the nodeinfo
    // with higher prio.
    $events[KernelEvents::VIEW][] = ['nodeinfo', 30];

    return $events;
  }

}
