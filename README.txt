CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers



INTRODUCTION
------------
Nodeinfo displays information about the currently viewed node in Drupal
messages area.

This is an administrative tool to allow admins and other privileged users
quick access to important information about currently viewed node. It is
not recommended to grant permissions to view the nodeinfo for non-authorized
visitors.



REQUIREMENTS
------------

This module has no dependencies to other modules



INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/node/1897420 for further information.

 * Add permissions for those roles you wish to display the nodeinfo:
   /admin/people/permissions



CONFIGURATION
-------------

Currently this module has no modifiable settings. There is no configuration.
When enabled and permission granted, users will se information about the
current node when viewing them.



TROUBLESHOOTING
---------------

 * If nodeinfo message is not displayed, check the following:

   - Is the "View node info" permissions enabled for the appropriate roles?
   
   - Does your template output the Drupal messages



MAINTAINERS
-----------

Current maintainers:
 * Timo Kirkkala (kirkkala) - https://www.drupal.org/user/390806
